# examonline
基于PHP的在线测评系统，目前在做最后的完善，完成后开源。
### 演示地址
> [地址](https://exam.vilson.xyz)  
> 账号：vilson，密码：123456
### 部分界面
![image](https://static.oschina.net/uploads/space/2018/0303/165934_nk8o_2461991.png)
![image](https://static.oschina.net/uploads/space/2018/0303/165937_4C58_2461991.png)
![image](https://static.oschina.net/uploads/space/2018/0303/165939_5Lgm_2461991.png)
![image](https://static.oschina.net/uploads/space/2018/0303/165942_acaE_2461991.png)
![image](https://static.oschina.net/uploads/space/2018/0303/165947_1JP2_2461991.png)
![image](https://static.oschina.net/uploads/space/2018/0303/165949_7Bqx_2461991.png)
![image](https://static.oschina.net/uploads/space/2018/0303/165951_alIy_2461991.png)
![image](https://static.oschina.net/uploads/space/2018/0303/165952_6Jyk_2461991.png)
![image](https://static.oschina.net/uploads/space/2018/0303/165954_QzWU_2461991.png)
